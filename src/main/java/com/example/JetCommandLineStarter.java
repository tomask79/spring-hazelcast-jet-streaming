package com.example;

import com.hazelcast.jet.*;
import com.hazelcast.jet.config.InstanceConfig;
import com.hazelcast.jet.config.JetConfig;
import com.hazelcast.jet.stream.IStreamMap;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Properties;

import static com.example.ReadRabbitMQP.readRabbitMQ;
import static com.hazelcast.jet.Edge.between;
import static com.hazelcast.jet.Processors.writeMap;
import static java.lang.Runtime.getRuntime;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * Created by tomask79 on 23.03.17.
 */
@Component
public class JetCommandLineStarter implements CommandLineRunner {
    private static final int MESSAGE_COUNT_PER_TOPIC = 1_000_000;

    @Override
    public void run(String... strings) throws Exception {
        System.out.println("########### RabbitMQ ingest into Hazelcast JET demo ##################");
        try {
            JetConfig cfg = new JetConfig();
            cfg.setInstanceConfig(new InstanceConfig().setCooperativeThreadCount(
                    Math.max(1, getRuntime().availableProcessors() / 2)));
            JetInstance instance = Jet.newJetInstance(cfg);

            IStreamMap<String, Integer> sinkMap = instance.getMap("sink");

            Job job = createJetJob(instance);

            long start = System.nanoTime();
            job.execute();
            while (true) {
                int mapSize = sinkMap.size();
                System.out.format("Received %d entries in %d milliseconds.%n",
                        mapSize, NANOSECONDS.toMillis(System.nanoTime() - start));
                if (mapSize == MESSAGE_COUNT_PER_TOPIC * 2) {
                    break;
                }
                Thread.sleep(100);
            }

            System.exit(0);

        } finally {
            Jet.shutdownAll();
        }
    }

    private static Properties props(String... kvs) {
        final Properties props = new Properties();
        for (int i = 0; i < kvs.length;) {
            props.setProperty(kvs[i++], kvs[i++]);
        }
        return props;
    }

    private static Job createJetJob(JetInstance instance) {
        DAG dag = new DAG();
        Properties props = props(
                "server", "localhost",
                "user", "guest",
                "password", "guest");
        Vertex source = dag.newVertex("source", readRabbitMQ(props, "jetInputQueue"));
        Vertex sink = dag.newVertex("sink", writeMap("sink"));
        dag.edge(between(source, sink));
        return instance.newJob(dag);
    }
}
