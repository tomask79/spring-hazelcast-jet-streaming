package com.example;

import com.hazelcast.jet.*;
import com.hazelcast.jet.impl.util.Util;
import com.hazelcast.nio.Address;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.*;
import java.util.function.Function;

import static com.hazelcast.jet.Traversers.traverseStream;
import static com.hazelcast.jet.stream.DistributedCollectors.toList;
import static java.util.stream.IntStream.range;
import static com.hazelcast.jet.Util.entry;

/**
 * Created by tomask79 on 25.03.17.
 */
public class ReadRabbitMQP<K, V> extends AbstractProcessor{
    private static final int POLL_TIMEOUT_MS = 100;
    private final Properties properties;
    private final String[] queueNames;
    private CachingConnectionFactory connectionFactory;
    private RabbitTemplate rabbitTemplate;
    private Traverser<Map.Entry<String, String>> traverser;

    private ReadRabbitMQP(String[] queueNames, Properties properties) {
        this.queueNames = queueNames;
        this.properties = properties;
    }

    public static ProcessorMetaSupplier readRabbitMQ(Properties properties, String... queues) {
        return new MetaSupplier<>(queues, properties);
    }

    @Override
    protected void init(Context context) throws Exception {
        // Connect to rabbitmq and subscribe to Queue.
        this.connectionFactory =
                new CachingConnectionFactory(properties.getProperty("server"));
        connectionFactory.setUsername(properties.getProperty("user"));
        connectionFactory.setPassword(properties.getProperty("password"));

        this.rabbitTemplate = new RabbitTemplate(connectionFactory);
        this.rabbitTemplate.setReceiveTimeout(POLL_TIMEOUT_MS);
        traverser = () -> null;
    }

    @Override
    public boolean isCooperative() {
        return false;
    }

    public void close() {
        connectionFactory.destroy();
    }

    @Override
    public boolean complete() {
        System.out.println("....Invoking RabbitMQ vertex processor complete...");
        if (emitCooperatively(traverser)) {
            final Message message =
                    this.rabbitTemplate.receive(this.queueNames[0]);
            if (message != null) {
                System.out.println("Message payload: " + new String(message.getBody()));
                final List<Message> list = new ArrayList<>();
                list.add(message);
                Random rn = new Random();
                traverser = traverseStream(list.stream()).map(r ->
                        entry(String.valueOf(rn.nextInt()), new String(r.getBody()))
                );
            }
        }
        return false;
    }

    private static final class MetaSupplier<K, V> implements ProcessorMetaSupplier {

        static final long serialVersionUID = 1L;
        private final String[] queueNames;
        private Properties properties;

        private MetaSupplier(String[] queueNames, Properties properties) {
            this.queueNames = queueNames;
            this.properties = properties;
        }

        @Override
        public Function<Address, ProcessorSupplier> get(List<Address> addresses) {
            return address -> new Supplier<>(queueNames, properties);
        }
    }

    private static class Supplier<K, V> implements ProcessorSupplier {

        static final long serialVersionUID = 1L;

        private final String[] queueNames;
        private final Properties properties;
        private transient List<Processor> processors;

        Supplier(String[] topicIds, Properties properties) {
            this.properties = properties;
            this.queueNames = topicIds;
        }

        @Override
        public List<Processor> get(int count) {
            return processors = range(0, count)
                    .mapToObj(i -> new ReadRabbitMQP<>(queueNames, properties))
                    .collect(toList());
        }

        @Override
        public void complete(Throwable error) {
            processors.stream()
                    .filter(p -> p instanceof ReadRabbitMQP)
                    .map(p -> (ReadRabbitMQP) p)
                    .forEach(p -> Util.uncheckRun(p::close));
        }
    }
}
